import { AbstractWorker, IQueueDefinition } from "@golemio/core/dist/integration-engine";
import { FatalError } from "@golemio/core/dist/shared/golemio-errors";

export type LoadModulesOutput = {
    [module: string]: {
        queueDefinitions: IQueueDefinition[];
        workers: Array<new () => AbstractWorker>;
    };
};

export class ModuleLoader {
    // See package.json for installed modules (@golemio/* packages)
    // and https://gitlab.com/operator-ict/golemio/code/modules for available modules
    private static modules = [
        "air-quality-stations",
        "bicycle-counters",
        "city-districts",
        "energetics",
        "fcd",
        "firebase-pid-litacka",
        "flow",
        "chmu",
        "gardens",
        "medical-institutions",
        "microclimate",
        "mobile-app-statistics",
        "mos",
        "municipal-authorities",
        "municipal-libraries",
        "municipal-police-stations",
        "ndic",
        "parkings",
        "playgrounds",
        "rush-hour-aggregation",
        "traffic-common",
        "vehiclesharing",
        "waste-collection",
        "waste-collection-yards",
        "waze-ccp",
        "waze-tt",
    ];

    public static async loadModules(): Promise<LoadModulesOutput> {
        let output: LoadModulesOutput = {};

        for (const module of ModuleLoader.modules) {
            const pkg = `@golemio/${module}/dist/integration-engine`;

            try {
                const { queueDefinitions, workers } = await import(pkg);
                output[module] = {
                    queueDefinitions: [],
                    workers: [],
                };
                if (queueDefinitions) {
                    output[module].queueDefinitions = queueDefinitions;
                }

                if (workers) {
                    output[module].workers = workers;
                }
            } catch (err) {
                throw new FatalError(`Cannot import module ${pkg}.`, "ModuleLoader", err);
            }
        }

        return output;
    }
}
