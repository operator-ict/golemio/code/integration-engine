# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.18.6] - 2025-03-12

-   No changelog

## [2.18.5] - 2025-03-11

### Removed

-   Remove sentry ([core#128](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/128))

## [2.18.4] - 2025-03-04

-   No changelog

## [2.18.3] - 2025-03-03

-   No changelog

## [2.18.2] - 2025-02-25

-   No changelog

## [2.18.1] - 2025-02-25

-   No changelog

## [2.18.0] - 2025-02-24

-   No changelog

## [2.17.7] - 2025-02-20

-   No changelog

## [2.17.6] - 2025-02-18

-   No changelog

## [2.17.5] - 2025-02-13

### Changed

-   ci/cd postgres image reference ([core#125](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/125))

## [2.17.4] - 2025-02-11

-   No changelog

## [2.17.3] - 2025-02-06

-   No changelog

## [2.17.2] - 2025-01-30

-   No changelog

## [2.17.1] - 2025-01-23

-   No changelog

## [2.17.0] - 2025-01-20

### Changed

-   Change TS build target from ES2015 to ES2021 ([core#121](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/121))

## [2.16.2] - 2025-01-14

-   No changelog

## [2.16.1] - 2025-01-07

-   No changelog

## [2.16.0] - 2024-12-17

### Changed

-   Upgrade Node.js to v20.18.0 ([core#118](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/118))
-   Disable Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

### Fixed

-   Memory leaks related to Sentry Express middleware ([p0255#90](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/90))

## [2.15.0] - 2024-12-12

### Changed

-   move golemio/cli to dev dependencies ([golemio-cli#3](https://gitlab.com/operator-ict/golemio/code/golemio-cli/-/issues/3))

## [2.14.5] - 2024-12-10

-   No changelog

## [2.14.4] - 2024-12-04

-   No changelog

## [2.14.3] - 2024-11-28

-   No changelog

## [2.14.2] - 2024-11-26

-   No changelog

## [2.14.1] - 2024-11-22

-   No changelog

## [2.14.0] - 2024-11-21

### Changed

-   Azure Blob Storage managed identity env vars update ([infra#304](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/304))

## [2.13.25] - 2024-11-14

-   No changelog

## [2.13.24] - 2024-11-05

-   No changelog

## [2.13.23] - 2024-10-24

-   add prefixes to asyncAPI

## [2.13.22] - 2024-10-22

-   No changelog

## [2.13.21] - 2024-10-16

### Added

-   asyncAPI upload documentation to azure ([integration-engine#267](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/267))

## [2.13.20] - 2024-10-15

-   No changelog

## [2.13.19] - 2024-09-26

-   No changelog

## [2.13.18] - 2024-09-24

-   No changelog

## [2.13.17] - 2024-09-19

-   No changelog

## [2.13.16] - 2024-09-12

-   No changelog

## [2.13.15] - 2024-09-10

### Added

-   Bedrichov measurements integration ([p0255#80](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/80))

## [2.13.14] - 2024-09-09

-   No changelog

## [2.13.13] - 2024-09-05

### Added

-   asyncAPI docs ([integration-engine#258](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/258))

## [2.13.12] - 2024-08-29

### Added

-   adding venzeo config ([p0149#308](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/308))

## [2.13.11] - 2024-08-27

-   No changelog

## [2.13.10] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [2.13.9] - 2024-08-13

-   No changelog

## [2.13.8] - 2024-08-01

-   No changelog

## [2.13.7] - 2024-07-30

-   No changelog

## [2.13.6] - 2024-07-29

-   No changelog

## [2.13.5] - 2024-07-23

-   No changelog

## [2.13.5] - 2024-07-17

-   Downgrade core to 1.11.1

## [2.13.4] - 2024-07-17

-   No changelog

## [2.13.3] - 2024-07-15

### Added

-   Add PPAS datasources for rest of the city apart from Prague 10 ([prevzeti-ed#22](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/22))

## [2.13.2] - 2024-07-10

-   No changelog

## [2.13.1] - 2024-07-04

-   No changelog

## [2.13.0] - 2024-06-24

-   No changelog

## [2.12.14] - 2024-06-19

-   No changelog

## [2.12.13] - 2024-06-17

-   No changelog

## [2.12.12] - 2024-06-05

-   No changelog

## [2.12.11] - 2024-06-03

-   No changelog

## [2.12.10] - 2024-05-29

-   No changelog

## [2.12.9] - 2024-05-27

-   No changelog

## [2.12.8] - 2024-05-22

-   No changelog

## [2.12.7] - 2024-05-20

-   No changelog

## [2.12.6] - 2024-05-14

-   No changelog

## [2.12.5] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [2.12.4] - 2024-05-06

-   No changelog

## [2.12.3] - 2024-04-29

### Added

-   Adding traffic-common migrations to pipeline ([fcd#12](https://gitlab.com/operator-ict/golemio/code/modules/fcd/-/issues/12))

## [2.12.2] - 2024-04-24

### Added

-   Add datasources Parking Prohibitions ([p0255#35](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/35))

## [2.12.1] - 2024-04-15

### Removed

-   Unused datasources-test

## [2.12.0] - 2024-04-10

### Added

-   Add datasources PRE Electro ([prevzeti-ed#5](https://gitlab.com/operator-ict/golemio/projekty/energetika/p0262.prevzeti-energeticke-databaze/-/issues/5)

## [2.11.11] - 2024-04-08

-   No changelog

## [2.11.10] - 2024-04-05

-   No changelog

## [2.11.9] - 2024-04-03

-   No changelog

## [2.11.8] - 2024-03-27

### Removed

-   Unused moduleConfig.template.json IPT OICT datasource entries ([parkings#309](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/merge_requests/309))

## [2.11.7] - 2024-03-25

-   No changelog

## [2.11.6] - 2024-03-20

-   No changelog

## [2.11.5] - 2024-03-18

-   No changelog

## [2.11.4] - 2024-03-11

-   No changelog

## [2.11.3] - 2024-03-06

### Added

-   Add datasources ([parkings#285](https://gitlab.com/operator-ict/golemio/code/modules/parkings/-/merge_requests/285)

## [2.11.2] - 2024-02-28

-   No changelog

## [2.11.1] - 2024-02-21

-   No changelog

## [2.11.0] - 2024-02-14

-   No changelog

## [2.10.12] - 2024-02-12

### Changed

-   Rename datasources ([core#75](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/75))

-   Modify module loader to improve logging ([core#92](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/92))

## [2.10.11] - 2024-01-29

-   No changelog

## [2.10.10] - 2024-01-29

### Changed

-   ArcGIS datasource template links ([general#530](https://gitlab.com/operator-ict/golemio/code/general/-/issues/530))

## [2.10.9] - 2024-01-24

-   No changelog

## [2.10.8] - 2024-01-23

### Changed

-   rollback vehicle-sharing to 2.4.1

## [2.10.7] - 2024-01-22

### Added

-   Smart4City parking integration datasource

## [2.10.6] - 2024-01-17

-   No changelog

## [2.10.5] - 2024-01-15

-   No changelog

## [2.10.4] - 2024-01-08

-   No changelog

## [2.10.3] - 2023-12-13

-   No changelog

## [2.10.2] - 2023-12-11

-   No changelog

## [2.10.1] - 2023-12-06

-   No changelog

## [2.10.0] - 2023-12-04

-   No changelog

## [2.9.17] - 2023-11-29

-   No changelog

## [2.9.16] - 2023-11-27

-   No changelog

## [2.9.15] - 2023-11-22

-   No changelog

## [2.9.14] - 2023-11-15

-   No changelog

## [2.9.13] - 2023-11-15

### Changed

-   Removing schema definitions ([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [2.9.12] - 2023-11-13

-   No changelog

## [2.9.11] - 2023-10-30

-   No changelog

## [2.9.10] - 2023-10-27

-   No changelog

## [2.9.9] - 2023-10-26

-   No changelog

## [2.9.8] - 2023-10-25

-   No changelog

## [2.9.7] - 2023-10-23

-   No changelog

## [2.9.6] - 2023-10-18

-   No changelog

## [2.9.5] - 2023-10-16

-   No changelog

## [2.9.4] - 2023-10-16

-   No changelog

## [2.9.3] - 2023-10-09

### Changed

-   CI/CD - Update Redis services to v7.2.1

## [2.9.2] - 2023-09-27

-   No changelog

## [2.9.1] - 2023-09-13

-   No changelog

## [2.9.0] - 2023-09-06

-   No changelog

## [2.8.14] - 2023-09-04

-   No changelog

## [2.8.13] - 2023-08-30

-   No changelog

## [2.8.12] - 2023-08-28

-   No changelog

## [2.8.11] - 2023-08-21

### Added

-   `RABBIT_CONSUMER_TIMEOUT` env for abortable workers

## [2.8.10] - 2023-08-16

-   No changelog

## [2.8.9] - 2023-08-16

-   No changelog

## [2.8.8] - 2023-08-09

-   No changelog

## [2.8.7] - 2023-08-07

-   No changelog

## [2.8.6] - 2023-08-02

-   No changelog

## [2.8.5] - 2023-07-31

### Fixed

-   Hotfix for parkomats

## [2.8.4] - 2023-07-31

### Changed

-   Use Node 18.17.0

## [2.8.3] - 2023-07-26

-   No changelog

## [2.8.2] - 2023-07-24

-   No changelog

## [2.8.1] - 2023-07-17

### Fixed

-   Opentelemetry initialization

## [2.8.0] - 2023-07-12

-   No changelog

## [2.7.12] - 2023-07-10

### Changed

-   Replaced Shared-Bikes with Vehiclesharing ([issue](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/15))

## [2.7.11] - 2023-07-04

-   No changelog

## [2.7.10] - 2023-06-28

-   No changelog

## [2.7.9] - 2023-06-26

-   No changelog

## [2.7.8] - 2023-06-26

-   No changelog

## [2.7.7] - 2023-06-21

-   No changelog

## [2.7.6] - 2023-06-19

### Removed

-   Remove BeRider integration ([p0131#136](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/136))

## [2.7.5] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [2.7.4] - 2023-06-12

-   No changelog

## [2.7.3] - 2023-06-12

-   No changelog

## [2.7.2] - 2023-06-07

-   No changelog

## [2.7.0] - 2023-06-05

-   No changelog

## [2.6.11] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [2.6.10] - 2023-05-29

-   No changelog

## [2.6.9] - 2023-05-25

-   No changelog

## [2.6.8] - 2023-05-22

-   No changelog

## [2.6.7] - 2023-05-18

-   No changelog

## [2.6.6] - 2023-05-17

-   No changelog

## [2.6.5] - 2023-05-15

-   No changelog

## [2.6.4] - 2023-05-03

-   No changelog

## [2.6.3] - 2023-04-26

### Removed

-   Mongo

## [2.6.2] - 2023-04-19

-   No changelog

## [2.6.1] - 2023-04-17

-   No changelog

## [2.6.0] - 2023-04-12

### Added

-   ITop configuration variables

### Changed

-   Removed modules: bicycle-parkings, parking-zones, public-toilets, shared-cars, meteosensors, traffic-cameras

## [2.5.11] - 2023-04-05

-   No changelog

## [2.5.10] - 2023-04-03

-   No changelog

## [2.5.9] - 2023-03-29

-   No changelog

## [2.5.8] - 2023-03-27

-   No changelog

## [2.5.7] - 2023-03-22

-   No changelog

## [2.5.6] - 2023-03-20

-   No changelog

## [2.5.5] - 2023-03-15

-   No changelog

## [2.5.4] - 2023-03-13

### Fixed

-   Update datasource template

## [2.5.3] - 2023-03-08

-   No changelog

## [2.5.2] - 2023-03-06

### Removed

-   Removed Sorted Waste Stations module ([sorted-waste-stations/#17](https://gitlab.com/operator-ict/golemio/code/modules/sorted-waste-stations/-/issues/17))

## [2.5.1] - 2023-03-01

-   No changelog

## [2.5.0] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [2.4.4] - 2023-02-23

-   No changelog

## [2.4.3] - 2023-02-22

-   No changelog

## [2.4.2] - 2023-02-20

### Added

-   Chmu module

## [2.4.1] - 2023-02-15

-   No changelog

## [2.4.0] - 2023-02-13

-   No changelog

## [2.3.2] - 2023-02-08

-   No changelog

## [2.3.1] - 2023-02-07

-   No changelog

## [2.3.0] - 2023-02-06

-   No changelog

## [2.2.2] - 2023-02-01

-   No changelog

## [2.2.1] - 2023-01-30

-   No changelog

## [2.2.0] - 2023-01-25

-   No changelog

## [2.1.0] - 2023-01-23

### Changed

-   Docker image optimization
-   Migrate to npm

## [2.0.55] - 2023-01-18

-   No changelog

## [2.0.54] - 2023-01-16

-   No changelog

## [2.0.53] - 2023-01-10

-   No changelog

## [2.0.52] - 2023-01-04

### Changed

-   update golemio core due to dependency injection implementation

## [2.0.51] - 2022-12-13

-   No changelog

## [2.0.50] - 2022-12-09

-   No changelog

## [2.0.49] - 2022-12-07

-   No changelog

## [2.0.48] - 2022-11-29

-   No changelog

## [2.0.47] - 2022-11-14

-   No changelog

## [2.0.46] - 2022-11-03

-   No changelog

## [2.0.45] - 2022-10-20

### Changed

-   Update TypeScript to v4.7.2, ts-node-dev to v2.0.0

## [2.0.44] - 2022-10-11

-   No changelog

## [2.0.43] - 2022-09-21

### Changed

-   Update Node.js to 16.17.0 ([code/general#418](https://gitlab.com/operator-ict/golemio/code/general/-/issues/418)

### Removed

-   All references to the pid module ([pid#174](https://gitlab.com/operator-ict/golemio/code/modules/pid/-/issues/174))
-   Unused dependencies ([modules/general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

